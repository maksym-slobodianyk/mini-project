import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import {
  Button,
  Form,
  Grid,
  Image,
  Input
} from 'semantic-ui-react';
import { bindActionCreators } from 'redux';
import AvatarProceeder from '../../components/AvatarProceeder';
import { updateAvatar, updateProfile } from './actions';

const Profile = ({ user, updateAvatar: updateProfileImg, updateProfile: updateProfileInfo }) => {
  const [editMode, setEditMode] = useState({ disabled: true });
  const [username, setUserName] = useState(user.username);
  const [status, setStatus] = useState(user.status);

  const disableEditMode = () => {
    setEditMode({ disabled: true });
    setUserName(user.username);
    setStatus(user.status);
  };
  const update = async () => {
    if (!username) {
      return;
    }
    try {
      await updateProfileInfo({ username, status });
      disableEditMode();
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <Image centered src={getUserImgLink(user.image)} size="medium" circular />
        <br />
        <br />
        <AvatarProceeder updateAvatar={updateProfileImg} />
        <br />
        <br />
        <Form onSubmit={update}>
          <span onDoubleClick={() => setEditMode({ disabled: false })}>
            <Form.Input
              icon="user"
              iconPosition="left"
              placeholder="Username"
              type="text"
              value={username}
              onChange={ev => setUserName(ev.target.value)}
              {...editMode}
            />
          </span>
          <span onDoubleClick={() => setEditMode({ disabled: false })}>
            <Form.Input
              icon="tag"
              iconPosition="left"
              placeholder="Status"
              type="text"
              value={status}
              onChange={ev => setStatus(ev.target.value)}
              {...editMode}
            />
          </span>
          <br />
          <br />
          <Input
            icon="at"
            iconPosition="left"
            placeholder="Email"
            type="email"
            disabled
            value={user.email}
          />
          <br />
          <br />
          {!editMode.disabled && (
            <div>
              <Button color="teal" type="submit">Save</Button>
              <Button onClick={() => setEditMode(disableEditMode)}>Cancel</Button>
            </div>
          )}
        </Form>
      </Grid.Column>
    </Grid>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  updateAvatar: PropTypes.func.isRequired,
  updateProfile: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {}
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

const actions = {
  updateAvatar,
  updateProfile
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
