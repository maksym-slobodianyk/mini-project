import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  DELETE_POST,
  EDIT_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EDITED_COMMENT,
  SET_EDITED_POST,
  SET_EXPANDED_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});
const deletePostAction = deletedId => ({
  type: DELETE_POST,
  deletedId
});
const editPostAction = post => ({
  type: EDIT_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

const setEditedPostAction = post => ({
  type: SET_EDITED_POST,
  post
});

const setEditedCommentAction = comment => ({
  type: SET_EDITED_COMMENT,
  comment
});
/*
const setCommentAction = comments => ({
  type: SET_COMMENTS,
  comments
});*/

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const deletePost = id => async dispatch => {
  const deletedId = await postService.deletePost(id);
  dispatch(deletePostAction(deletedId));
};

export const editPost = post => async dispatch => {
  await postService.editPost(post);
  const editedPost = await postService.getPost(post.id);
  dispatch(editPostAction(editedPost));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const toggleEditedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setEditedPostAction(post));
};

export const toggleEditedComment = commentId => async dispatch => {
  const comment = commentId ? await commentService.getComment(commentId) : undefined;
  dispatch(setEditedCommentAction(comment));
};

export const likePost = postId => async (dispatch, getRootState) => {
  const result = await postService.likePost(postId);
  const { profile: { user } } = getRootState();
  const diff = result?.id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

  const mapReaction = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diff, // diff is taken from the current closure
    dislikeCount: Number(result?.isNew === false ? post.dislikeCount - 1 : post.dislikeCount),
    likedUsers: (diff === 1)
      ? [{ ...user, avatar: user.image }, ...post.likedUsers]
      : [...post.likedUsers.filter(a => a.id !== user.id)],
    dislikedUsers: (result?.isNew) ? post.dislikedUsers : post.dislikedUsers.filter(a => a.id !== user.id)
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapReaction(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapReaction(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const result = await postService.dislikePost(postId);
  const { profile: { user } } = getRootState();
  const diff = result?.id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const mapReaction = post => ({
    ...post,
    dislikeCount: Number(post.dislikeCount) + diff, // diff is taken from the current closure
    likeCount: Number(result?.isNew === false ? post.likeCount - 1 : post.likeCount),
    dislikedUsers: (diff === 1)
      ? [{ ...user, avatar: user.image }, ...post.dislikedUsers]
      : [...post.dislikedUsers.filter(a => a.id !== user.id)],
    likedUsers: (result?.isNew) ? post.likedUsers : post.likedUsers.filter(a => a.id !== user.id)
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapReaction(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapReaction(expandedPost)));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const likeComment = commentId => async (dispatch, getRootState) => {
  const result = await commentService.reactOnComment(commentId, true);
  const { profile: { user } } = getRootState();
  const diff = result?.id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

  const mapReaction = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + diff, // diff is taken from the current closure
    dislikeCount: Number(result?.isNew === false ? comment.dislikeCount - 1 : comment.dislikeCount),
    likedUsers: (diff === 1)
      ? [{ ...user, avatar: user.image }, ...comment.likedUsers]
      : [...comment.likedUsers.filter(a => a.id !== user.id)],
    dislikedUsers: (result?.isNew) ? comment.dislikedUsers : comment.dislikedUsers.filter(a => a.id !== user.id)
  });

  const { posts: { expandedPost } } = getRootState();
  const updatedPost = { ...expandedPost,
    comments: expandedPost.comments.map(comment => (comment.id !== commentId ? comment : mapReaction(comment))) };
  if (expandedPost) {
    dispatch(setExpandedPostAction(updatedPost));
  }
};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
  const result = await commentService.reactOnComment(commentId, false);
  const { profile: { user } } = getRootState();
  const diff = result?.id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const mapReaction = comment => ({
    ...comment,
    dislikeCount: Number(comment.dislikeCount) + diff, // diff is taken from the current closure
    likeCount: Number(result?.isNew === false ? comment.likeCount - 1 : comment.likeCount),
    dislikedUsers: (diff === 1)
      ? [{ ...user, avatar: user.image }, ...comment.dislikedUsers]
      : [...comment.dislikedUsers.filter(a => a.id !== user.id)],
    likedUsers: (result?.isNew) ? comment.likedUsers : comment.likedUsers.filter(a => a.id !== user.id)
  });

  const { posts: { expandedPost } } = getRootState();
  const updatedPost = { ...expandedPost,
    comments: expandedPost.comments.map(comment => (comment.id !== commentId ? comment : mapReaction(comment))) };
  if (expandedPost) {
    dispatch(setExpandedPostAction(updatedPost));
  }
};

export const deleteComment = id => async (dispatch, getRootState) => {
  const { posts: { posts, expandedPost } } = getRootState();

  const deletedId = await commentService.deleteComment(id);
  const reduceCommentsNumber = post => ({
    ...post,
    commentCount: Number(post.commentCount) - 1,
    comments: post.comments?.filter(comment => comment.id !== deletedId)
  });

  const updatedComments = posts.map(post => (post.id !== expandedPost.id ? post : reduceCommentsNumber(post)));
  dispatch(setPostsAction(updatedComments));
  if (expandedPost) {
    dispatch(setExpandedPostAction(reduceCommentsNumber(expandedPost)));
  }
};

export const editComment = comment => async (dispatch, getRootState) => {
  const { posts: { expandedPost } } = getRootState();
  await commentService.editComment(comment);
  const editedComment = await commentService.getComment(comment.id);
  const updatedComments = expandedPost.comments.map(c => (c.id !== comment.id ? c : editedComment));
  dispatch(setExpandedPostAction({ ...expandedPost, comments: updatedComments }));
};
