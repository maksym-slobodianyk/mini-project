/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader, Button, Segment, Icon } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  addPost,
  deletePost,
  editPost,
  toggleEditedPost
} from './actions';
import EditPost from '../../components/EditPost';
import styles from './styles.module.scss';
import EditComment from '../../components/EditComment';
import { updateAvatar } from '../Profile/actions';

const postsFilter = {
  userId: undefined,
  getMyPosts: false,
  getOthersPosts: false,
  getLikedPosts: false,
  from: 0,
  count: 10
};

const Thread = ({
  userId,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  editedPost,
  editedComment,
  hasMorePosts,
  addPost: createPost,
  deletePost: deleteP,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  toggleEditedPost: toggleEdit
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [showOthersPosts, setShowOthersPosts] = useState(false);
  const [showLikedPosts, setShowLikedPosts] = useState(false);
  const [showFilter, setShowFilter] = useState(false);

  const loadWithFilter = () => {
    postsFilter.userId = userId;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll*/
  };

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    setShowOthersPosts(false);
    postsFilter.getMyPosts = !showOwnPosts;
    postsFilter.getOthersPosts = false;
    loadWithFilter();
  };
  const toggleShowOthersPosts = () => {
    setShowOthersPosts(!showOthersPosts);
    setShowOwnPosts(false);
    postsFilter.getOthersPosts = !showOthersPosts;
    postsFilter.getMyPosts = false;
    loadWithFilter();
  };
  const toggleLikedPosts = () => {
    setShowLikedPosts(!showLikedPosts);
    postsFilter.getLikedPosts = !showLikedPosts;
    loadWithFilter();
  };

  const toggleFilter = () => {
    setShowFilter(!showFilter);
  };

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <Segment>
        <Button animated="vertical" color="teal" fluid size="large" onClick={toggleFilter}>
          <Button.Content hidden>Filter</Button.Content>
          <Button.Content visible>
            <Icon name="filter" />
          </Button.Content>
        </Button>
        {showFilter && (
          <div>
            <div className={styles.toolbar}>
              <Checkbox
                toggle
                label="Show only my posts"
                checked={showOwnPosts}
                onChange={toggleShowOwnPosts}
              />
            </div>
            <div className={styles.toolbar}>
              <Checkbox
                toggle
                label="Hide my posts"
                checked={showOthersPosts}
                onChange={toggleShowOthersPosts}
              />
            </div>
            <div className={styles.toolbar}>
              <Checkbox
                toggle
                label="Liked by me"
                checked={showLikedPosts}
                onChange={toggleLikedPosts}
              />
            </div>
          </div>
        )}
      </Segment>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            post={post}
            profileId={userId}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            toggleEditedPost={toggleEdit}
            sharePost={sharePost}
            key={post.id}
            deletePost={deleteP}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} />}
      {editedPost && <EditPost post={editedPost} uploadImage={uploadImage} />}
      {sharedPostId && <SharedPostLink postId={sharedPostId} close={() => setSharedPostId(undefined)} />}
      {editedComment && <EditComment comment={editedComment} />}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  editedPost: PropTypes.objectOf(PropTypes.any),
  editedComment: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  toggleEditedPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  editedPost: undefined,
  editedComment: undefined,
  userId: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  editedPost: rootState.posts.editedPost,
  editedComment: rootState.posts.editedComment,
  userId: rootState.profile.user.id
});

const actions = {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  toggleEditedPost,
  addPost,
  deletePost,
  editPost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
