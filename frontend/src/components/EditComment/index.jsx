import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Modal, Segment } from 'semantic-ui-react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { editComment, toggleEditedComment } from '../../containers/Thread/actions';

const EditComment = ({
  comment,
  editComment: edit,
  toggleEditedComment: toggle
}) => {
  const [body, setBody] = useState(comment.body);

  const handleEditComment = async () => {
    if (!body) {
      return;
    }
    await edit({ id: comment.id, body, postId: comment.postId, createdAt: comment.createdAt });
    toggle();
    setBody('');
  };
  return (
    <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
      <Segment>
        <Form reply onSubmit={handleEditComment}>
          <Form.TextArea
            value={body}
            placeholder="Type a comment..."
            onChange={ev => setBody(ev.target.value)}
          />
          <Button type="submit" content="Submit" labelPosition="left" icon="edit" primary />
        </Form>
      </Segment>
    </Modal>
  );
};

EditComment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  editComment: PropTypes.func.isRequired,
  toggleEditedComment: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  comment: rootState.posts.editedComment
});

const actions = { editComment, toggleEditedComment };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditComment);
