import React, { useState } from 'react';
import { Button, Modal, Segment } from 'semantic-ui-react';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import PropTypes from 'prop-types';

const AvatarProceeder = ({ updateAvatar }) => {
  const [cropProps, setCropProps] = useState({ unit: 'px', width: 200, height: 200, aspect: 1 });
  const [imageUrl, setImageUrl] = useState(undefined);
  const [imageFile, setImageFile] = useState(undefined);
  const [isUploading, setIsUploading] = useState(false);

  const onCrop = crop => {
    setCropProps(crop);
  };

  const handleUploadFile = async () => {
    setIsUploading(true);
    if (cropProps.width !== 0 && cropProps.height !== 0) {
      try {
        await updateAvatar(
          { file: imageFile,
            x: cropProps.x,
            y: cropProps.y,
            width: cropProps.width,
            height: cropProps.height
          }
        );
      } finally {
        // TODO: show error
        setIsUploading(false);
        setImageFile(undefined);
        setImageUrl(undefined);
      }
    }
  };

  const handlePreview = ({ target }) => {
    if (target.files[0]) {
      setImageUrl(URL.createObjectURL(target.files[0]));
      setImageFile(target.files[0]);
    }
  };

  return (
    <div>
      <Button color="teal" icon as="label">
        Change profile picture
        <input name="image" type="file" onChange={handlePreview} hidden />
      </Button>
      {imageUrl && (
        <Modal
          dimmer="blurring"
          centered={false}
          open
          onClose={() => { setImageFile(undefined); setImageUrl(undefined); }}
        >
          <Segment>
            <ReactCrop
              src={imageUrl}
              crop={cropProps}
              onChange={onCrop}
              maxWidth={200}
              maxHeight={200}
            />
          </Segment>
          <Segment>
            <Button content="Upload" onClick={handleUploadFile} loading={isUploading} disabled={isUploading} />
          </Segment>
        </Modal>
      )}
    </div>
  );
};

AvatarProceeder.propTypes = {
  updateAvatar: PropTypes.func.isRequired
};

export default AvatarProceeder;
