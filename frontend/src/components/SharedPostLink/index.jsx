import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { Modal, Input, Icon } from 'semantic-ui-react';
import validator from 'validator';
import styles from './styles.module.scss';
import * as postService from '../../services/postService';

const SharedPostLink = ({ postId, close }) => {
  const [copied, setCopied] = useState(false);
  const [email, setEmail] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isEmailValid, setIsEmailValid] = useState(true);
  let input = useRef();

  const copyToClipboard = e => {
    input.select();
    document.execCommand('copy');
    e.target.focus();
    setCopied(true);
  };

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const shareByEmail = async () => {
    if (!isEmailValid || isLoading) {
      return;
    }
    setIsLoading(true);
    await postService.shareByEmail(postId, email);
    setIsLoading(false);
    setEmail('');
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Share Post</span>
        {copied && (
          <span>
            <Icon color="green" name="copy" />
            Copied
          </span>
        )}
      </Modal.Header>
      <Modal.Content>
        <Input
          fluid
          action={{
            color: 'teal',
            labelPosition: 'right',
            icon: 'copy',
            content: 'Copy',
            onClick: copyToClipboard
          }}
          value={`${window.location.origin}/share/${postId}`}
          ref={ref => { input = ref; }}
        />
        <br />
        <Input
          fluid
          action={{
            color: 'teal',
            labelPosition: 'right',
            icon: 'mail',
            content: 'Share by email',
            onClick: shareByEmail
          }}
          value={email}
          onChange={ev => emailChanged(ev.target.value)}
          onBlur={() => setIsEmailValid(validator.isEmail(email))}
          ref={ref => { input = ref; }}
        />
      </Modal.Content>
    </Modal>
  );
};

SharedPostLink.propTypes = {
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired
};
export default SharedPostLink;
