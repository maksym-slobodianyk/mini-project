import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon, Image, Label, List, Popup } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from './styles.module.scss';
import {
  deleteComment,
  dislikeComment,
  likeComment,
  toggleEditedComment
} from '../../containers/Thread/actions';

const Comment = ({
  comment: { id, body, likeCount, dislikeCount, createdAt, user, likedUsers, dislikedUsers },
  likeComment: like,
  dislikeComment: dislike,
  deleteComment: deleteC,
  toggleEditedComment: toggle,
  userId
}) => (
  <CommentUI className={styles.comment}>
    <CommentUI.Avatar src={getUserImgLink(user.image)} />
    <CommentUI.Content>
      <CommentUI.Author as="a">
        {user.username}
      </CommentUI.Author>
      <CommentUI.Metadata>
        {moment(createdAt).fromNow()}
      </CommentUI.Metadata>
      <div>
        <CommentUI.Metadata>
          {user.status}
        </CommentUI.Metadata>
      </div>
      <CommentUI.Text>
        {body}
      </CommentUI.Text>
      <CommentUI.Actions>
        <CommentUI.Action>
          <Popup
            mouseEnterDelay={750}
            offset="50px, 50px"
            position="bottom center"
            trigger={(
              <Label basic size="small" className={styles.toolbarBtn} onClick={() => like(id)}>
                <Icon name="thumbs up" />
                {likeCount}
              </Label>
            )}
          >
            <Popup.Header>People who liked</Popup.Header>
            <Popup.Content>
              <List>
                {likedUsers.map(likedUser => (
                  <List.Item key={likedUser.id}>
                    <Image src={getUserImgLink(likedUser.avatar)} avatar />
                    <List.Content>
                      <List.Header>
                        {likedUser.username}
                      </List.Header>
                      <List.Description>
                        {likedUser.status}
                      </List.Description>
                    </List.Content>
                  </List.Item>
                ))}
              </List>
            </Popup.Content>
          </Popup>
          <Popup
            mouseEnterDelay={750}
            position="bottom center"
            hideOnScroll
            trigger={(
              <Label basic size="small" className={styles.toolbarBtn} onClick={() => dislike(id)}>
                <Icon name="thumbs down" />
                {dislikeCount}
              </Label>
            )}
          >
            <Popup.Header>People who disliked</Popup.Header>
            <Popup.Content>
              <List>
                {dislikedUsers.map(dislikedUser => (
                  <List.Item key={dislikedUser.id}>
                    <Image src={getUserImgLink(dislikedUser.avatar)} avatar />
                    <List.Content>
                      <List.Header>
                        {dislikedUser.username}
                      </List.Header>
                      <List.Description>
                        {dislikedUser.status}
                      </List.Description>
                    </List.Content>
                  </List.Item>
                ))}
              </List>
            </Popup.Content>
          </Popup>
          {(userId === user.id) && (
            <span>
              <Label basic size="small" as="b" className={styles.toolbarBtn} onClick={() => toggle(id)}>
                <Icon name="edit" />
              </Label>
              <Label basic size="small" as="b" className={styles.toolbarBtn} onClick={() => deleteC(id)}>
                <Icon name="trash alternate" />
              </Label>
            </span>
          )}
        </CommentUI.Action>
      </CommentUI.Actions>
    </CommentUI.Content>
  </CommentUI>
);

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  toggleEditedComment: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = {
  likeComment,
  dislikeComment,
  deleteComment,
  toggleEditedComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Comment);
