import React, { useEffect, useState } from 'react';
import Logo from 'src/components/Logo';
import {
  Grid,
  Header,
  Form,
  Button,
  Segment
} from 'semantic-ui-react';
import { Redirect } from 'react-router-dom';
import * as authService from '../../services/authService';

const PasswordRestorePage = match => {
  const [password, setPassword] = useState('');
  const [token, setToken] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isPasswordValid, setIsPasswordValid] = useState(true);
  const [redirect, setRedirect] = useState(false);

  useEffect(() => {
    setToken(match.match.params.token);
    console.log(token);
  });

  const passwordChanged = data => {
    setPassword(data);
    setIsPasswordValid(true);
  };

  const handleRestoreClick = async () => {
    if (!isPasswordValid || isLoading || !token) {
      return;
    }
    setIsLoading(true);
    try {
      await authService.restorePassword({ password, token });
      setRedirect(true);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        <Header as="h2" color="teal" textAlign="center">
          Restore your password
        </Header>
        <Form name="loginForm" size="large" onSubmit={handleRestoreClick}>
          <Segment>
            <Form.Input
              fluid
              icon="lock"
              iconPosition="left"
              placeholder="Password"
              type="password"
              error={!isPasswordValid}
              onChange={ev => passwordChanged(ev.target.value)}
              onBlur={() => setIsPasswordValid(Boolean(password))}
            />
            <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
              Submit new password
            </Button>
          </Segment>
        </Form>
      </Grid.Column>
      {redirect && (
        <Redirect to="/" />
      )}
    </Grid>
  );
};

PasswordRestorePage.propTypes = {

};

export default PasswordRestorePage;
