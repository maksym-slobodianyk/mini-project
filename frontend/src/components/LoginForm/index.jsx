import React, { useState } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { Form, Button, Segment, Modal } from 'semantic-ui-react';
import * as authService from '../../services/authService';

const LoginForm = ({ login }) => {
  const [email, setEmail] = useState('');
  const [restoreEmail, setRestoreEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [isRestoreEmailValid, setIsRestoreEmailValid] = useState(true);
  const [isPasswordValid, setIsPasswordValid] = useState(true);
  const [inRestoreMode, setInRestoreMode] = useState(false);
  const [restoreIsLoading, setRestoreIsLoading] = useState(false);

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const redirectEmailChanged = data => {
    setRestoreEmail(data);
    setIsRestoreEmailValid(true);
  };

  const passwordChanged = data => {
    setPassword(data);
    setIsPasswordValid(true);
  };

  const handleLoginClick = async () => {
    const isValid = isEmailValid && isPasswordValid;
    if (!isValid || isLoading) {
      return;
    }
    setIsLoading(true);
    try {
      await login({ email, password });
    } catch {
      // TODO: show error
      setIsLoading(false);
    }
  };

  const handleRestoreClick = async () => {
    if (!isRestoreEmailValid || restoreIsLoading) {
      return;
    }
    setRestoreIsLoading(true);
    try {
      await authService.sendRestoreRequest(restoreEmail);
    } finally {
      setRestoreIsLoading(false);
    }
  };

  const toggleRestoreMode = () => {
    if (!restoreIsLoading) {
      setInRestoreMode(!inRestoreMode);
    }
  };

  return (
    <Form name="loginForm" size="small" onSubmit={handleLoginClick}>
      <Segment>
        <Form.Input
          fluid
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          error={!isEmailValid}
          onChange={ev => emailChanged(ev.target.value)}
          onBlur={() => setIsEmailValid(validator.isEmail(email))}
        />
        <Form.Input
          fluid
          icon="lock"
          iconPosition="left"
          placeholder="Password"
          type="password"
          error={!isPasswordValid}
          onChange={ev => passwordChanged(ev.target.value)}
          onBlur={() => setIsPasswordValid(Boolean(password))}
        />
        <span style={{ cursor: 'pointer' }} onClick={toggleRestoreMode}>Forgot your password?</span>
        <br />
        <br />
        <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
          Login
        </Button>
        {inRestoreMode && (
          <Modal dimmer="blurring" centered={false} open onClose={toggleRestoreMode}>
            <Modal.Header>
              Please fill in the email that you used to register.
              We will send you an email with instructions on how to reset your password.
            </Modal.Header>
            <Segment>
              <Form onSubmit={handleRestoreClick}>
                <Form.Input
                  fluid
                  icon="mail"
                  iconPosition="left"
                  placeholder="Email"
                  type="email"
                  error={!isPasswordValid}
                  onChange={ev => redirectEmailChanged(ev.target.value)}
                  onBlur={() => setIsPasswordValid(Boolean(password))}
                />
                <Button type="submit" color="teal" fluid size="large" loading={restoreIsLoading} primary>
                  Send email
                </Button>
              </Form>
            </Segment>

          </Modal>
        )}
      </Segment>
    </Form>
  );
};

LoginForm.propTypes = {
  login: PropTypes.func.isRequired
};

export default LoginForm;
