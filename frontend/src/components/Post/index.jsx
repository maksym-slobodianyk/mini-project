import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Popup, List } from 'semantic-ui-react';
import moment from 'moment';

import styles from './styles.module.scss';
import PostDropdown from '../PostDropdown';
import { getUserImgLink } from '../../helpers/imageHelper';

const Post = ({ post, profileId, likePost, dislikePost, toggleExpandedPost, toggleEditedPost,
  sharePost, deletePost }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = moment(createdAt).fromNow();
  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>

          {(profileId === user.id) && (
            <PostDropdown
              postId={id}
              deletePost={deletePost}
              editPost={toggleEditedPost}
            />
          )}
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Popup
          mouseEnterDelay={750}
          position="bottom center"
          trigger={(
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
              <Icon name="thumbs up" />
              {likeCount}
            </Label>
          )}
        >
          <Popup.Header>People who liked</Popup.Header>
          <Popup.Content>
            <List>
              {post.likedUsers.map(likedUser => (
                <List.Item key={likedUser.id}>
                  <Image src={getUserImgLink(likedUser.avatar)} avatar />
                  <List.Content>
                    <List.Header>
                      {likedUser.username}
                    </List.Header>
                    <List.Description>
                      {likedUser.status}
                    </List.Description>
                  </List.Content>
                </List.Item>
              ))}
            </List>
          </Popup.Content>
        </Popup>
        <Popup
          mouseEnterDelay={750}
          position="bottom center"
          trigger={(
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
              <Icon name="thumbs down" />
              {dislikeCount}
            </Label>
          )}
        >
          <Popup.Header>People who disliked</Popup.Header>
          <Popup.Content>
            <List>
              {post.dislikedUsers.map(dislikedUser => (
                <List.Item key={dislikedUser.id}>
                  <Image src={getUserImgLink(dislikedUser.avatar)} avatar />
                  <List.Content>
                    <List.Header>
                      {dislikedUser.username}
                    </List.Header>
                    <List.Description>
                      {dislikedUser.status}
                    </List.Description>
                  </List.Content>
                </List.Item>
              ))}
            </List>
          </Popup.Content>
        </Popup>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  profileId: PropTypes.string.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleEditedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

export default Post;
