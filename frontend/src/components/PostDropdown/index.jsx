import React from 'react';
import { Dropdown } from 'semantic-ui-react';
import PropTypes from 'prop-types';

const PostDropdown = ({ postId, deletePost, editPost }) => (
  <Dropdown
    icon="dropdown"
    floating
    labeled
    className="icon"
  >
    <Dropdown.Menu>
      <Dropdown.Item icon="edit" text="Edit post" onClick={() => editPost(postId)} />
      <Dropdown.Item icon="trash alternate" text="Delete post" onClick={() => deletePost(postId)} />
    </Dropdown.Menu>
  </Dropdown>
);
PostDropdown.propTypes = {
  postId: PropTypes.string.isRequired,
  deletePost: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired
};
export default PostDropdown;
