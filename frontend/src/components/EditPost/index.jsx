import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Icon, Image, Segment, Modal } from 'semantic-ui-react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from './styles.module.scss';
import { editPost, toggleEditedPost } from '../../containers/Thread/actions';

const EditPost = ({
  post,
  editPost: edit,
  uploadImage: uploadImg,
  toggleEditedPost: toggle
}) => {
  const [body, setBody] = useState(post.body);
  const [image, setImage] = useState(post.image);
  const [isUploading, setIsUploading] = useState(false);
  const handleEditPost = async () => {
    if (!body) {
      return;
    }
    await edit({ id: post.id, imageId: image?.id, body, createdAt: post.createdAt });
    setBody('');
    setImage(undefined);
    toggle(null);
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id, link } = await uploadImg(target.files[0]);
      setImage({ id, link });
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  return (
    <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
      <Segment>
        <Form onSubmit={handleEditPost}>
          <Form.TextArea
            name="body"
            value={body}
            placeholder="What is the news?"
            onChange={ev => setBody(ev.target.value)}
          />
          {image?.link && (
            <div className={styles.imageWrapper}>
              <Image className={styles.image} src={image?.link} alt="post" />
            </div>
          )}
          <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
            <Icon name="image" />
            Attach image
            <input name="image" type="file" onChange={handleUploadFile} hidden />
          </Button>
          <Button floated="right" color="blue" type="submit">Submit</Button>
        </Form>
      </Segment>
    </Modal>
  );
};

EditPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  editPost: PropTypes.func.isRequired,
  toggleEditedPost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.editedPost
});

const actions = { editPost, toggleEditedPost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditPost);
