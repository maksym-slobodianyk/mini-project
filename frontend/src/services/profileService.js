import callWebApi from 'src/helpers/webApiHelper';

export const updateUser = async request => {
  const response = await callWebApi({
    endpoint: '/api/user',
    type: 'PUT',
    request
  });
  return response.json();
};
