import callWebApi from 'src/helpers/webApiHelper';

export const uploadImage = async (image, x = 0, y = 0, width = 0, height = 0) => {
  const response = await callWebApi({
    endpoint: `/api/images?x=${x}&y=${y}&width=${width}&height=${height}`,
    type: 'POST',
    attachment: image
  });
  return response.json();
};
