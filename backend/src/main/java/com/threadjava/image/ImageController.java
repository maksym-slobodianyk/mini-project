package com.threadjava.image;

import com.threadjava.image.dto.ImageDto;
import com.threadjava.image.dto.ImageUploadDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api/images")
public class ImageController {
    @Autowired
    ImageService imageService;

    @PostMapping
    public ImageDto post(
            @RequestParam("image") MultipartFile file,
            @RequestParam("x") Integer x,
            @RequestParam("y") Integer y,
            @RequestParam("width") Integer width,
            @RequestParam("height") Integer height
    ) throws IOException {
        return imageService.upload(new ImageUploadDto(file, x, y, width, height));
    }
}
