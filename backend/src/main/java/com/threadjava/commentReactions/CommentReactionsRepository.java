package com.threadjava.commentReactions;

import com.threadjava.commentReactions.model.CommentReaction;
import com.threadjava.postReactions.model.PostReaction;
import com.threadjava.users.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CommentReactionsRepository extends CrudRepository<CommentReaction, UUID> {
    @Query("SELECT r " +
            "FROM CommentReaction r " +
            "WHERE r.user.id = :userId AND r.comment.id = :commentId ")
    Optional<CommentReaction> getCommentReaction(@Param("userId") UUID userId, @Param("commentId") UUID commentId);

    @Query("SELECT r.user " +
            "FROM CommentReaction r " +
            "WHERE r.comment.id = :commentId AND r.isLike = :isLike ")
    List<User> findUsersByCommentIdAndReaction(@Param("commentId") UUID commentId, @Param("isLike") Boolean isLike);
}