package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentEditDto;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.post.dto.PostCreationResponseDto;
import com.threadjava.post.dto.PostEditDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.UUID;
import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/comments")
public class CommentController {
    @Autowired
    private CommentService commentService;

    @GetMapping("/{id}")
    public CommentDetailsDto get(@PathVariable UUID id) {
        return commentService.getCommentById(id);
    }

    @PostMapping
    public CommentDetailsDto post(@RequestBody CommentSaveDto commentDto) {
        commentDto.setUserId(getUserId());
        return commentService.create(commentDto);
    }

    @DeleteMapping("/{id}")
    public @ResponseBody
    UUID delete(@PathVariable UUID id) {
        var deletedId = commentService.softDelete(id);
        return deletedId;
    }

    @PutMapping
    public CommentDetailsDto update(@RequestBody CommentEditDto comment) {
        System.out.println(comment);
        comment.setUserId(getUserId());
        return commentService.update(comment);
    }


}
