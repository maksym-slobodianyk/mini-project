package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentEditDto;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.comment.model.Comment;
import com.threadjava.commentReactions.CommentReactionsRepository;
import com.threadjava.post.PostMapper;
import com.threadjava.post.PostsRepository;
import com.threadjava.post.dto.PostCommentDto;
import com.threadjava.post.dto.PostCreationResponseDto;
import com.threadjava.post.dto.PostEditDto;
import com.threadjava.post.model.Post;
import com.threadjava.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.UUID;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private CommentReactionsRepository commentReactionsRepository;

    public CommentDetailsDto getCommentById(UUID id) {
        return commentRepository.findCommentById(id)
                .map(CommentMapper.MAPPER::commentQueryToCommentDetailsDto)
                .map(this::setReactedUsersToComment)
                .orElseThrow();
    }

    public CommentDetailsDto create(CommentSaveDto commentDto) {
        var comment = CommentMapper.MAPPER.commentSaveDtoToModel(commentDto);
        var postCreated = commentRepository.save(comment);
        return CommentMapper.MAPPER.commentToCommentDetailsDto(postCreated);
    }

    public UUID softDelete(UUID postID) {
        commentRepository.deleteById(postID);
        return postID;
    }

    public CommentDetailsDto update(CommentEditDto comment) {
        Comment s = CommentMapper.MAPPER.commentEditDtoToModel(comment);
        Comment postEdited = commentRepository.save(s);
        return CommentMapper.MAPPER.commentToCommentDetailsDto(postEdited);
    }

    private CommentDetailsDto setReactedUsersToComment(CommentDetailsDto comment) {
        var likedUsers = commentReactionsRepository.findUsersByCommentIdAndReaction(comment.getId(), true);
        var dislikedUsers = commentReactionsRepository.findUsersByCommentIdAndReaction(comment.getId(), false);
        comment.setDislikedUsers(dislikedUsers);
        comment.setLikedUsers(likedUsers);
        return comment;
    }
}
