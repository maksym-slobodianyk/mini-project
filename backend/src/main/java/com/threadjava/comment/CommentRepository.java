package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsQueryResult;
import com.threadjava.comment.model.Comment;
import com.threadjava.post.dto.PostDetailsQueryResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CommentRepository extends JpaRepository<Comment, UUID> {
    @Query("SELECT new com.threadjava.comment.dto.CommentDetailsQueryResult(p.id, p.body, p.user, " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.comment = p), " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.comment = p), " +
            "p.post.id, p.createdAt, p.updatedAt ) " +
            "FROM Comment p " +
            "WHERE p.post.id = :postId")
    List<CommentDetailsQueryResult> findAllByPostId(UUID postId);

    @Query("SELECT new com.threadjava.comment.dto.CommentDetailsQueryResult(p.id, p.body, p.user, " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.comment = p), " +
            "(SELECT COALESCE(SUM(CASE WHEN pr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.comment = p), " +
            "p.post.id, p.createdAt, p.updatedAt ) " +
            "FROM Comment p " +
            "WHERE p.id = :id")
    Optional<CommentDetailsQueryResult> findCommentById(@Param("id") UUID id);
}