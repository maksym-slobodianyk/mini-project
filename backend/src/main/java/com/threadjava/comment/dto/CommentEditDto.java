package com.threadjava.comment.dto;

import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class CommentEditDto {
    private UUID id;
    private String body;
    private UUID postId;
    private UUID userId;
    private Date createdAt;
}
