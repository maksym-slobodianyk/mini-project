package com.threadjava.auth;

import com.threadjava.auth.dto.*;
import com.threadjava.users.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private AuthService authService;
    @Autowired
    private SimpMessagingTemplate template;

    @PostMapping("/register")
    public AuthUserDTO signUp(@RequestBody UserRegisterDto user) throws Exception {
        return authService.register(user);
    }

    @PostMapping("/login")
    public AuthUserDTO login(@RequestBody UserLoginDTO user) throws Exception {
        return authService.login(user);
    }

    @PostMapping("/restore")
    public Object post(@RequestParam("email") String email) {
        try {
            authService.sendRestoreLink(email);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("/password")
    public Object post(@RequestBody PasswordRestoreDto restoreDto) {
        authService.restorePassword(restoreDto);
        return null;
    }

}
