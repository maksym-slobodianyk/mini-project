package com.threadjava.auth.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class PasswordRestoreDto {
    private String password;
    private UUID token;
}