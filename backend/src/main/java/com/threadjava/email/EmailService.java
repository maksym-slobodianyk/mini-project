package com.threadjava.email;

import com.threadjava.post.PostsService;
import com.threadjava.post.dto.PostDetailsDto;
import com.threadjava.postReactions.PostReactionService;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import com.threadjava.postReactions.model.PostReaction;
import com.threadjava.users.UsersService;
import com.threadjava.users.dto.UserDetailsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;


@Service
public class EmailService {

    @Autowired
    public JavaMailSender emailSender;

    @Autowired
    public UsersService usersService;

    @Autowired
    public PostsService postsService;

    @Autowired
    public PostReactionService postReactionService;

    public void sendMessage(
            String mailTo, String subject, String message) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(mailTo);
        mailMessage.setSubject(subject);
        mailMessage.setText(message);
        emailSender.send(mailMessage);
    }

    public void sendReactionNotificationEmail(ResponsePostReactionDto reactionDto) {
        final PostDetailsDto post = postsService.getPostById(reactionDto.getPostId());
        final UserDetailsDto sender = usersService.getUserById(reactionDto.getUserId());
        final UserDetailsDto receiver = usersService.getUserById(post.getUser().getId());
        final String reaction = reactionDto.getIsLike() ? "liked" : "disliked";
        final String userName = sender.getUsername();
        final String postLink = "http://localhost:3001/share/" + post.getId();
        final String theme = reactionDto.getIsLike() ? "Perfect news for you! @Thread" : "Bad news for you :( @Thread";
        final String message = String.format("Hello,\n\nYour post was %s by %s!\n\nPost: %s", reaction, userName, postLink);
        sendMessage(receiver.getEmail(), theme, message);
    }

    public void sendPostShareEmail(UUID userId, UUID postId, String email) {
        final PostDetailsDto post = postsService.getPostById(postId);
        final UserDetailsDto sender = usersService.getUserById(userId);
        final String userName = sender.getUsername();
        final String postLink = "http://localhost:3001/share/" + post.getId();
        final String theme = String.format("%s shared a post with you! @Thread",userName);
        final String message = String.format("Hello,\n\n%s shared a post with you!\n\nPost link: %s", userName, postLink);
        sendMessage(email, theme, message);
    }

    public void sendPasswordRestoreEmail(String email, String link) {
        final String theme = "Restore password @Thread";
        final String message = String.format("Hello,\n\nClick on the link below to restore your password!\n\nRestore link: %s", link);
        sendMessage(email, theme, message);
    }

}
