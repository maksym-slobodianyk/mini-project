package com.threadjava.users;

import com.threadjava.users.dto.UserDetailsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UsersService userDetailsService;
    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping
    public UserDetailsDto getUser() {
        return userDetailsService.getUserById(getUserId());
    }

    @PutMapping
    public UserDetailsDto updateUser(@RequestBody UserDetailsDto user) {
        user.setId(getUserId());
        try {
            return userDetailsService.updateUser(user);
        } catch (Exception e) {
            template.convertAndSend("/topic/profile", "User with such name already exist!");
            throw e;
        }
    }
}
