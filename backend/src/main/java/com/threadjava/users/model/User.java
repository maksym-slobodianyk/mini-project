package com.threadjava.users.model;

import com.threadjava.db.BaseEntity;
import com.threadjava.image.model.Image;
import jdk.jfr.Unsigned;
import lombok.*;
import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
@EqualsAndHashCode(callSuper=true)
@Table(name = "users")
public class User extends BaseEntity {
    @Column(name = "email", unique=true)
    private String email;

    @Column(name = "username", unique = true)
    private String username;

    @Column(name = "status")
    private String status;

    @Column(name = "password")
    private String password;

    @Column(name = "password_restore_token")
    private UUID restoreToken;

    @ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "avatar_id")
    private Image avatar;
}
