package com.threadjava.users;

import com.threadjava.auth.dto.PasswordRestoreDto;
import com.threadjava.auth.model.AuthUser;
import com.threadjava.users.dto.UserDetailsDto;
import com.threadjava.users.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UsersService implements UserDetailsService {
    @Autowired
    private UsersRepository usersRepository;

    @Override
    public AuthUser loadUserByUsername(String email) throws UsernameNotFoundException {
        return usersRepository
                .findByEmail(email)
                .map(user -> new AuthUser(user.getId(), user.getEmail(), user.getPassword()))
                .orElseThrow(() -> new UsernameNotFoundException(email));
    }

    public UserDetailsDto getUserById(UUID id) {
        return usersRepository
                .findById(id)
                .map(user -> UserMapper.MAPPER.userToUserDetailsDto(user))
                .orElseThrow(() -> new UsernameNotFoundException("No user found with username"));
    }

    public UserDetailsDto updateUser(UserDetailsDto userDto) {
        User user = UserMapper.MAPPER.userDetailsDtoToUser(userDto);
        User userToUpadate = usersRepository.getOne(userDto.getId());
        userToUpadate.setAvatar(user.getAvatar());
        userToUpadate.setUsername(user.getUsername());
        userToUpadate.setUsername(user.getUsername());
        userToUpadate.setStatus(user.getStatus());
        User editedUser = usersRepository.save(userToUpadate);
        return UserMapper.MAPPER.userToUserDetailsDto(editedUser);
    }

    public void setPasswordRestoreToken(String email, UUID token) throws IllegalAccessException {
        Optional<User> userToUpadate = usersRepository.findByEmail(email);
        if (userToUpadate.isEmpty()) throw new IllegalAccessException();
        userToUpadate.get().setRestoreToken(token);
        User editedUser = usersRepository.save(userToUpadate.get());
    }

    public UserDetailsDto resetPassword(PasswordRestoreDto restoreDto) throws IllegalArgumentException {
        Optional<User> userToUpadate = usersRepository.findAll().stream().filter(a -> a.getRestoreToken() != null).filter(a -> a.getRestoreToken().equals(restoreDto.getToken())).findFirst();
        // Optional<User> userToUpadate = users.stream().filter(a -> a.getRestoreToken().equals(restoreDto.getToken()));
       if(userToUpadate.isEmpty()) throw  new IllegalArgumentException();
        userToUpadate.get().setRestoreToken(null);
        userToUpadate.get().setPassword(restoreDto.getPassword());
        User editedUser = usersRepository.save(userToUpadate.get());
        return null;
    }

    public void save(User user) {
        usersRepository.save(user);
    }
}