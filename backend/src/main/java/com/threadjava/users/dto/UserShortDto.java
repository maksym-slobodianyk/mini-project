package com.threadjava.users.dto;

import com.threadjava.image.model.Image;
import lombok.*;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserShortDto {
    private UUID id;
    private String username;
    private String status;
    private Image image;
}
