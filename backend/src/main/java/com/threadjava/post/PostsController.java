package com.threadjava.post;


import com.threadjava.post.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/posts")
public class PostsController {
    @Autowired
    private PostsService postsService;
    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping
    public List<PostListDto> get(@RequestParam(defaultValue = "0") Integer from,
                                 @RequestParam(defaultValue = "10") Integer count,
                                 @RequestParam(defaultValue = "449f0416-14a3-4e9e-98e5-7bf86afe0bb3",required = false) UUID userId,
                                 @RequestParam(defaultValue = "false",required = false) Boolean getMyPosts,
                                 @RequestParam(defaultValue = "false",required = false) Boolean getOthersPosts,
                                 @RequestParam(defaultValue = "false",required = false) Boolean getLikedPosts) {
        return postsService.getAllPosts(from, count, userId,getMyPosts, getOthersPosts, getLikedPosts);
    }

    @GetMapping("/{id}")
    public PostDetailsDto get(@PathVariable UUID id) {
        return postsService.getPostById(id);
    }

    @PostMapping
    public PostCreationResponseDto post(@RequestBody PostCreationDto postDto) {
        postDto.setUserId(getUserId());
        var item = postsService.create(postDto);
        template.convertAndSend("/topic/new_post", item);
        return item;
    }

    @DeleteMapping("/{id}")
    public @ResponseBody
    UUID delete(@PathVariable UUID id) {
        var deletedId = postsService.softDelete(id);
        return deletedId;
    }

    @PutMapping
    public PostCreationResponseDto update(@RequestBody PostEditDto postDto) {
        postDto.setUserId(getUserId());
        var item = postsService.update(postDto);
        return item;
    }

    @PostMapping("/share")
    public Object post(@RequestParam("postId") UUID postId, @RequestParam("email") String email) {
        postsService.share(getUserId(), postId, email);
        return null;
    }

}
