package com.threadjava.post;

import com.threadjava.comment.CommentRepository;
import com.threadjava.commentReactions.CommentReactionsRepository;
import com.threadjava.email.EmailService;
import com.threadjava.post.dto.*;
import com.threadjava.post.model.Post;
import com.threadjava.postReactions.PostReactionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PostsService {
    @Autowired
    private PostsRepository postsCrudRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private CommentReactionsRepository commentReactionsRepository;
    @Autowired
    private PostReactionsRepository postReactionsRepository;
    @Autowired
    private EmailService emailService;

    public List<PostListDto> getAllPosts(Integer from, Integer count, UUID userId, Boolean getMyPosts, Boolean getOthersPosts, Boolean getLikedPosts) {
        var pageable = PageRequest.of(from / count, count);
        return postsCrudRepository
                .findAllPosts(userId, getMyPosts, getOthersPosts, getLikedPosts, pageable)
                .stream()
                .map(PostMapper.MAPPER::postListToPostListDto)
                .map(this::setReactedUsers)
                .collect(Collectors.toList());
    }

    public PostDetailsDto getPostById(UUID id) {
        var post = postsCrudRepository.findPostById(id)
                .map(PostMapper.MAPPER::postToPostDetailsDto)
                .orElseThrow();

        var comments = commentRepository.findAllByPostId(id)
                .stream()
                .map(PostMapper.MAPPER::commentToCommentDto)
                .map(this::setReactedUsersToComment)
                .collect(Collectors.toList());
        post.setComments(comments);

        var likedUsers = postReactionsRepository.findUsersByPostIdAndReaction(id, true);
        var dislikedUsers = postReactionsRepository.findUsersByPostIdAndReaction(id, false);

        post.setDislikedUsers(dislikedUsers);
        post.setLikedUsers(likedUsers);
        System.out.println(post);
        return post;
    }

    public PostCreationResponseDto create(PostCreationDto postDto) {
        Post post = PostMapper.MAPPER.postDetailsDtoToPost(postDto);
        Post postCreated = postsCrudRepository.save(post);
        return PostMapper.MAPPER.postToPostCreationResponseDto(postCreated);
    }

    public UUID softDelete(UUID postID) {
        postsCrudRepository.deleteById(postID);
        return postID;
    }

    public PostCreationResponseDto update(PostEditDto post) {
        Post s = PostMapper.MAPPER.postEditDtoToPost(post);
        Post postEdited = postsCrudRepository.save(s);
        return PostMapper.MAPPER.postToPostCreationResponseDto(postEdited);
    }

    public void share(UUID userId, UUID postId, String email) {
        emailService.sendPostShareEmail(userId, postId, email);
    }

    private PostListDto setReactedUsers(PostListDto post) {
        var likedUsers = postReactionsRepository.findUsersByPostIdAndReaction(post.getId(), true);
        var dislikedUsers = postReactionsRepository.findUsersByPostIdAndReaction(post.getId(), false);
        post.setDislikedUsers(dislikedUsers);
        post.setLikedUsers(likedUsers);
        return post;
    }

    private PostCommentDto setReactedUsersToComment(PostCommentDto comment) {
        var likedUsers = commentReactionsRepository.findUsersByCommentIdAndReaction(comment.getId(), true);
        var dislikedUsers = commentReactionsRepository.findUsersByCommentIdAndReaction(comment.getId(), false);
        comment.setDislikedUsers(dislikedUsers);
        comment.setLikedUsers(likedUsers);
        return comment;
    }
}
