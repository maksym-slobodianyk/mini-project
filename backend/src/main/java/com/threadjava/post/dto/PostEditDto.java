package com.threadjava.post.dto;

import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class PostEditDto {
    private UUID id;
    private String body;
    private UUID imageId;
    private UUID userId;
    private Date createdAt;
}
