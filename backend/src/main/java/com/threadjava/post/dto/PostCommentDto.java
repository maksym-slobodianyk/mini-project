package com.threadjava.post.dto;

import com.threadjava.users.dto.UserShortDto;
import com.threadjava.users.model.User;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
public class PostCommentDto {
    private UUID id;
    private String body;
    private UserShortDto user;
    private long likeCount;
    private long dislikeCount;
    private UUID postId;
    private Date createdAt;
    private Date updatedAt;
    private List<User> likedUsers;
    private List<User> dislikedUsers;
}
